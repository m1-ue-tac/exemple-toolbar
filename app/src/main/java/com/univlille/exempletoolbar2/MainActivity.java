package com.univlille.exempletoolbar2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.appbar.MaterialToolbar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MaterialToolbar topAppBar = findViewById(R.id.top_app_bar);

        topAppBar.setNavigationOnClickListener(view -> {
            Toast.makeText(this, "menu principal", Toast.LENGTH_SHORT).show();
        });

        topAppBar.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.favorite:
                    Toast.makeText(this, "favorite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.search:
                    Toast.makeText(this, "search", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.more:
                    Toast.makeText(this, "more", Toast.LENGTH_SHORT).show();
                    return true;
                default:
                    Toast.makeText(this, "autre !", Toast.LENGTH_SHORT).show();
                    return false;

            }
        });
    }
}